/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excepciones;

import java.util.Scanner;

/**
 *
 * @author Esteban
 */
public class ClaseDivicion {

    public static void main(String[] args) {

        try {
            int val1, val2, result;
            Scanner sc = new Scanner(System.in);

            System.out.println("Ingrese el primer valor");
            val1 = sc.nextInt();

            System.out.println("Ingrese el segundo valor");
            val2 = sc.nextInt();

            result = val1 / val2;

            System.out.println("Divicion: " + result);
        } catch (Exception e) {

            System.out.println("Algo salio mal "+e);
        }

    }
}
