/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analizador;

/**
 *
 * @author Esteban
 */
public class Pila {
    private Nodo ultimoValor;
    
    
    public Pila(){
        ultimoValor = null;
        
    }
    
    //Metodo para insertar en la pila
    public void Insertar(char valor){
        
        Nodo nuevoNodo = new Nodo();
        
        nuevoNodo.informacion = valor;
        
        if(ultimoValor == null){
            
            nuevoNodo.siguinete = null;
            ultimoValor = nuevoNodo;
            
        }else{
            
            nuevoNodo.siguinete = ultimoValor;
            ultimoValor = nuevoNodo;
        }
    }
    
    public char Extraer(){
        
        if(ultimoValor != null){
        
            char informacion = ultimoValor.informacion;
            ultimoValor = ultimoValor.siguinete;
            return informacion;
            
        }else{
            return Character.MAX_VALUE;
        }
    }
    
    
    //Metodo para saber si la pila esta vacia
    public boolean PilaVacia(){
        return ultimoValor == null;
    }
}
