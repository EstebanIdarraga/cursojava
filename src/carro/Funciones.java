/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carro;

import java.util.Scanner;

/**
 *
 * @author Esteban
 */
public abstract class Funciones {

    protected Scanner sc = new Scanner(System.in);
    protected int accion;
    private static int estado;
    protected String matricula = "ER96";
    protected String color = "Negro";
    protected String propietario = "Esteban";

    //Menu de Opciones
    public void Menu() {

        int iniciador = 0;

        while (iniciador == 0) {
            System.out.println("");
            System.out.println("******************************");
            System.out.println("*      -- /Bienvenido/ --     *");
            System.out.println("*  Por favor elija una opcion *");
            System.out.println("*                             *");
            System.out.println("* 1. Informacion del Vehiculo *");
            System.out.println("*          2. Encender        *");
            System.out.println("*          3. Apagar          *");
            System.out.println("*          4. Acelerar        *");
            System.out.println("*          5. Frenar          *");
            System.out.println("*          6. Pitar           *");
            System.out.println("*          7. Salir           *");
            System.out.println("*******************************");
            System.out.println("");

            accion = sc.nextInt();

            if (accion >= 1 && accion <= 7) {

                if (accion == 1) {
                    System.out.println(" --------------------------");
                    System.out.println("   Infor. del Vehiculo ");
                    System.out.println("                       ");
                    System.out.println(" Propietario: " + propietario);
                    System.out.println(" Color: " + color);
                    System.out.println(" Matricula: " + matricula);
                    System.out.println(" Estado: " + estado);
                    System.out.println(" --------------------------");

                } else if (accion == 2) {
                    if (estado == 0) {
                        Encender obj = new Encender();
                        obj.Tarea();
                    } else {
                        System.out.println("");
                        System.out.println("****************************");
                        System.out.println("* El Autoya esta encendido *");
                        System.out.println("****************************");
                        System.out.println("");
                    }

                } else if (accion == 3) {

                    if (estado == 0) {
                        alertaApagado();
                    } else {
                        Apagar obj = new Apagar();
                        obj.Tarea();
                    }
                } else if (accion == 4) {
                    if (estado == 0) {
                        alertaApagado();
                    } else {
                        Acelerar obj = new Acelerar();
                        obj.Tarea();
                    }
                } else if (accion == 5) {
                    if (estado == 0) {
                        alertaApagado();
                    } else {
                        Frenar obj = new Frenar();
                        obj.Tarea();
                    }
                } else if (accion == 6) {

                    if (estado == 0) {
                        alertaApagado();
                    } else {
                        Pitar obj = new Pitar();
                        obj.Tarea();
                    }
                } else if (accion == 7) {
                    System.out.println("///////////////////////");
                    System.out.println("/  Hasta la proxima   /");
                    System.out.println("///////////////////////");
                    iniciador = 1;

                }
            } else {
                System.out.println("/--------------------------/");
                System.out.println("/**** Opcion no valida ****/");
                System.out.println("/--------------------------/");
            }
        }
    }

    public abstract void Tarea();

    public void alertaApagado() {
        System.out.println("");
        System.out.println("************************");
        System.out.println("* El Auto esta apagado *");
        System.out.println("************************");
        System.out.println("");
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int newEstado) {
        this.estado = newEstado;
    }
}
