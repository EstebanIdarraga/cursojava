/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listastipocola;
import java.util.Scanner;
import javax.swing.JOptionPane;
/**
 *
 * @author Esteban
 */
public class ClasePrincipal {
    public static void main(String[] args){
    
        Scanner sc = new Scanner(System.in);
        int opcion = 0, nodoInformacion = 0;
        
        Cola cola = new Cola();
        
        while(opcion != 4){
        
            try{
            
                opcion = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "Menu de Opciones\n\n"
                        +"1. Insertar un nodo\n"
                        +"2. Extraer un nodo\n"
                        +"3. Mostrar contenido\n"
                        +"4. Salir\n\n"));
                
                
                switch(opcion){
                    case 1:
                         nodoInformacion = Integer.parseInt(JOptionPane.showInputDialog(null, "Por favor ingresa el valor a guardar en el nodo"));
                         cola.Insertar(nodoInformacion);
                        break;
                    case 2:
                        if(!cola.ColaVacia()){
                        
                            JOptionPane.showMessageDialog(null, "Se extrajo un nodo con el valor: "+cola.Extraer());
                        }else{
                        
                            JOptionPane.showMessageDialog(null, "La cola esta vacia");
                        }
                        break;
                    case 3:
                        cola.Mostrar();
                        break;
                    case 4:
                        opcion = 4;
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Opcion no disponible");
                        break;      
                }
            }catch(NumberFormatException e){
            
            
            }
        }
    }
}
