/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listastipocola;

import javax.swing.JOptionPane;

/**
 *
 * @author Esteban
 */
public class Cola {
    
    private Nodo inicioCola, finalCola;
    String cola = "";
    
    public Cola(){
        
        inicioCola = null;
        finalCola = null;  
    }
    
    
    public boolean ColaVacia(){
        
        if(inicioCola == null){
            return true;
        }else{
            return false;
        }
    }
    
    public void Insertar(int informacion){
        Nodo nuevoNodo = new Nodo();
        
        nuevoNodo.informacion = informacion;
        nuevoNodo.siguiente = null;
        
        if(ColaVacia()){
        
            inicioCola = nuevoNodo;
            finalCola = nuevoNodo;
        }else{
            finalCola.siguiente = nuevoNodo;
            finalCola = nuevoNodo;
        }
    }
    
    
    //Metodo para extraer de la cola
    public int Extraer(){
    
        if(!ColaVacia()){
        
                int informacion = inicioCola.informacion;
                
                if(inicioCola == finalCola){
                
                    inicioCola = null;
                    finalCola = null;
                }else{
                
                  inicioCola = inicioCola.siguiente;                    
                }
            return informacion;
        }else{
            
          return Integer.MAX_VALUE;  
            
        }
    }
    
    //Metodo para mostrar el contenido de la cola
    public void Mostrar(){
    
        Nodo recorrido = inicioCola;
        String colaInvertida = "";
        
        while(recorrido != null){
        
            cola +=recorrido.informacion + " ";
            recorrido = recorrido.siguiente;
        }
        
        String cadena [] = cola.split(" ");
        
        for(int i = cadena.length - 1; i >= 0; i--){
            colaInvertida += " " + cadena[i];
        }
        
        JOptionPane.showMessageDialog(null, colaInvertida);
        cola = "";
    
    }
    
    
}
