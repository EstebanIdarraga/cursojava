/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cohete;

import java.util.Scanner;

/**
 *
 * @author Esteban
 */
public abstract class ClasePadre {
    
    private Scanner sc = new Scanner(System.in);
    private int opcion = 0;
    private static int estadoCohete = 0;
    protected static int modulos = 1;
    
    
    public void Menu(){
        int iniciador = 0;
        int finalizador = 0;
        
        
        while(finalizador != 1){
        System.out.println("************************");
        System.out.println("*        > NASA <      *");
        System.out.println("************************");
        System.out.println("*  Control de Cohete   *");
        System.out.println("*                      *");
        System.out.println("*  1. Encender Motores *");
        System.out.println("*  2. Separar soportes *");
        System.out.println("*  3. Despegar         *");
        System.out.println("*  4. Separar nave     *");
        System.out.println("*  5. Aterrizar        *");
        System.out.println("*  6. Terminar Mision  *");
        System.out.println("************************");
        System.out.println("");
        System.out.print("Elija una accion: ");
        opcion = sc.nextInt();
        
        switch(opcion){
            
            case 1:
                ClasePadre mensajero = new EstadoCohete();
                mensajero.Accion();
                break;
            case 2:
                ClasePadre mensajero2 = new Separar();
                mensajero2.Accion();
                break;
            case 3:
                ClasePadre mensajero3 = new Despegue();
                mensajero3.Accion();
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                System.out.println("Bien");
                finalizador = 1;
                break;
            default:
                System.out.println("Opcion no valida");
                break;           
        }
        
        }
    }
    
    public abstract void Accion();
    
    public int getEstadoCohete(){
        return estadoCohete;
    }
    
    public void setEstadoCohete(int newEstadoCohete){
        this.estadoCohete = newEstadoCohete;
    }
    
    public int getModulo(){
        return modulos;
    }
    
    public void setModulo(int newModulo){
        this.modulos = newModulo;
    }
    

}
