/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Factorial;

/**
 *
 * @author Esteban
 */
public class Factorial {
    
    
    public int Calcular(int parametro){
        
        if(parametro > 0){
            int valorCalculado = parametro * Calcular(parametro - 1);
            return valorCalculado;
        }
        
        return 1;
    }
}
