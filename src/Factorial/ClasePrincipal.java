/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Factorial;
import java.util.Scanner;
/**
 *
 * @author Esteban
 */
public class ClasePrincipal {
    public static void main(String[] args){
        
        int entrada = 0;
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Ingresa un numero para calcular su factorial");
        entrada = sc.nextInt();
        
        Factorial obj = new Factorial();
        int factorial = obj.Calcular(entrada);
        
        
        System.out.println("El factorial de "+entrada+" es "+factorial);
    }
}
