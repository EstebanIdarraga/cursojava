/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasemath;

/**
 *
 * @author Esteban
 */
public class FuncionesTrigonometricas {
    public static void main(String[] args){
    
        double result = 0;
        double anguloGrados = 45;
        double anguloRadianes = Math.toRadians(anguloGrados);
        
        //Seno
         result = Math.sin(anguloRadianes);
         
         System.out.println("Seno de "+ anguloGrados+ " es "+result);
         
         //Coseno
         result = Math.cos(anguloRadianes);
         System.out.println("Coseno de "+anguloGrados+" es "+result);
         
         //Tangente
         result = Math.tan(anguloRadianes);
         System.out.println("La Tangente de "+anguloGrados+" es "+result);
    }
}
