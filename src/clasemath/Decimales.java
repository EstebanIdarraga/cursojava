/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasemath;

import java.text.DecimalFormat;

/**
 *
 * @author Esteban
 */
public class Decimales {

    public static void main(String[] args) {

        double numero = 2;
        double raiz = Math.sqrt(numero);
        System.out.println("La raiz de " + numero + " es " + raiz);

        //Limitar decimales con la clase DecimalFormat        
        DecimalFormat df = new DecimalFormat("#.00");
        System.out.println("La raiz de " + numero + " es " + df.format(raiz));

        //Utilizando String format
        System.out.println("La raiz de " + numero + " es " + String.format("%.2f", raiz));

        //Utilizando la clase Math.round
        System.out.println("La raiz de " + numero + " es " + (double)Math.round(raiz * 100d) / 100);

    }
}
