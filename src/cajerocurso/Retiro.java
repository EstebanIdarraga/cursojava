/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajerocurso;

/**
 *
 * @author Esteban
 */
public class Retiro extends ClasePadre{
    
    @Override
    public void Transacciones(){
        System.out.println("Cuanto deseas retirar");
        Retirar();
        
        if(retiro <= getSaldo()){
            transaccion = getSaldo();
            setSaldo(transaccion - retiro);
            
            System.out.println("------------------------------");
            System.out.println("Retiraste: "+ retiro);
            System.out.println("Saldo actual: "+getSaldo());
            System.out.println("------------------------------");
        }else{
            System.out.println("---------------------");
            System.out.println("Saldo insuficiente.");
            System.out.println("---------------------");
        }
    }
}
