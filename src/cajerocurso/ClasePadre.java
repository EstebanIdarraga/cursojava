/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajerocurso;
import java.util.Scanner;

/**
 *
 * @author Esteban
 */
public abstract class ClasePadre {

    protected Scanner sc = new Scanner(System.in);
    protected int transaccion, retiro, deposito;
    private static int saldo;

    public void Menu() {
        int bandera = 0;
        int opcion = 0;

        //Menu de seleccion
        while (bandera == 0) {

            System.out.println("***************************");
            System.out.println("*        Bienvenido       *");
            System.out.println("***************************");
            System.out.println("*   Elija una operacion   *");
            System.out.println("*                         *");
            System.out.println("* 1. Consultar saldo      *");
            System.out.println("* 2. Retiro de efectivo   *");
            System.out.println("* 3. Deposito de efectivo *");
            System.out.println("* 4. Salir                *");
            System.out.println("*                         *");
            System.out.println("***************************");
            System.out.println("");
            opcion = sc.nextInt();

            if (opcion >= 1 && opcion <= 4) {
                if (opcion == 1) {
                    ClasePadre mensajero = new Consultar();
                    mensajero.Transacciones();                  
                } else if (opcion == 2) {
                    ClasePadre mensajero = new Retiro();
                    mensajero.Transacciones();
                } else if (opcion == 3) {
                    ClasePadre mensajero = new Depositar();
                    mensajero.Transacciones();
                } else if (opcion == 4) {
                    System.out.println("************************");
                    System.out.println("*Gracias, vuelva pronto*");
                    System.out.println("************************");
                    bandera = 1;
                }
            } else {
                System.out.println(" ----------------");
                System.out.println("|Opcion no valida|");
                System.out.println(" ----------------");
            }
        }
    }

    //Metodo para retirar dinero
    protected void Retirar() {
        retiro = sc.nextInt();
    }

    // Metodo para depositar dinero
    protected void Depositar() {
        deposito = sc.nextInt();
    }
    
    
    //Metodo abstracto
    public abstract void Transacciones();
    
    
    //Metodo getter
    public int getSaldo(){
      return saldo;
    }
    
    //Metodo Setter
    public void setSaldo(int newSaldo){
      this.saldo = newSaldo;
    }

}
