/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pilas;

import javax.swing.JOptionPane;

/**
 *
 * @author Esteban
 */
public class Pila {
    
    private Nodo ultimoValor;
    int tamano = 0;
    String lista = "";
    
    public Pila(){
        ultimoValor = null;
        tamano = 0;
    }
    
    public boolean PilaVacia(){
        
        return ultimoValor == null;
        
    }
    
    public void InsertarNodo(int nodo){
        
        Nodo nuevoNodo = new Nodo(nodo);
        nuevoNodo.siguiente = ultimoValor;
        ultimoValor = nuevoNodo;
        tamano++;
    }
    
    
    public int EliminarNodo(){
        int auxiliar = ultimoValor.informacion;
        ultimoValor = ultimoValor.siguiente;
        tamano--;
        
        return auxiliar;
    }
    
    public int MostarUltimoValorIngresado(){
        return ultimoValor.informacion;
    }
    
    public int TamanoPila(){
        return tamano;
    }
    
    public void VaciarPila(){
        while(!PilaVacia()){
            EliminarNodo();
        }
    }
    
    public void MostrarValores(){
        Nodo recorrido = ultimoValor;
        
        while(recorrido != null){
            lista += recorrido.informacion + "\n";
            recorrido = recorrido;           
        }
        
        JOptionPane.showMessageDialog(null, lista);
        lista = "";
    }
}
