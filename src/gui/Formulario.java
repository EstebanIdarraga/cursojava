/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author Esteban
 */
public class Formulario extends JFrame implements ActionListener{
    
   private JTextField txtField;
   private JLabel label1;
   private JButton btn1;
   
   public Formulario(){
   
       setLayout(null);
       setDefaultCloseOperation(EXIT_ON_CLOSE);
       
       label1 = new JLabel("Mensaje: ");
       label1.setBounds(15, 10, 100, 30);
       add(label1);
       
       txtField = new JTextField();
       txtField.setBounds(80, 16, 190, 20);
       add(txtField);
       
       btn1 = new JButton("Mostrar");
       btn1.setBounds(10, 60, 100, 30);
       btn1.addActionListener(this);
       add(btn1);      
   }
   
   
   public void actionPerformed(ActionEvent e){
       if(e.getSource() == btn1){ 
           
           String cadena = txtField.getText();
           JOptionPane.showMessageDialog(null, cadena);
       }
   }
   
   
   public static void main(String[] args){
   
       Formulario form = new Formulario();
       
       form.setBounds(0, 0, 300, 150);
       form.setVisible(true);
       form.setLocationRelativeTo(null);
       form.setResizable(false);
   }
}
